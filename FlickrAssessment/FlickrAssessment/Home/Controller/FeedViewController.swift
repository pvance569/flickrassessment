//
//  FeedViewController.swift
//  FlickrAssessment
//
//  Created by Patel, Virenkumar on 11/17/19.
//  Copyright © 2019 Patel, Virenkumar. All rights reserved.
//

import UIKit
import SDWebImage

class FeedViewController: UIViewController {

    @IBOutlet weak var feedTitleLabel: UILabel!
    @IBOutlet weak var feedImage: UIImageView!
    @IBOutlet weak var authorLabel: UILabel!
    
    var currentIndex: Int?
    var feedTitle : String?
    var authorName: String?
    var feedImageName : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        authorLabel.text = authorName
        feedTitleLabel.text = feedTitle
        self.feedImage.sd_setImage(with: URL(string: feedImageName ?? ""), placeholderImage: UIImage(named: "icon_image_coming_soon"), options: [], context: nil)
        
        // Do any additional setup after loading the view.
    }
    

}
