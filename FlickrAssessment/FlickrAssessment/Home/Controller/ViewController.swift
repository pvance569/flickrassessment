//
//  ViewController.swift
//  FlickrAssessment
//
//  Created by Patel, Virenkumar on 11/17/19.
//  Copyright © 2019 Patel, Virenkumar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var contentView: UIView!
    var currentViewControllerIndex: Int = 0
    
    var viewModel: FlickrFeedViewModel? {
        didSet {
            DispatchQueue.main.async {
                self.configurePageViewController()
            }
        }
    }
    
    // Will use this one to determine where to fetch the data from. MockData(for testing) or from APIs?
    var fetchDAO: FlickrFeedResultsProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.fetchFeeds(dataAccessObjet: self.fetchDAO ?? FlickrFeedDAO())
    }
    
    // Fetch the feeds based on the DAO
    func fetchFeeds(dataAccessObjet: FlickrFeedResultsProtocol) {
        dataAccessObjet.flickrFeedResults { (flickrFeed) in
            if let feed = flickrFeed {
                self.viewModel = FlickrFeedViewModel(feed: feed)
            } else {
                self.displayError()
            }
        }
    }
    
    // Display error when fetching the data get fail.
    fileprivate func displayError() {
        let alertController = UIAlertController(title: nil, message: "Service is temporary unavailable. Please try again later", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // Configure the PageViewController in ViewController's View.
    func configurePageViewController() {
        
        guard let pageViewController = storyboard?.instantiateViewController(identifier: "CustomPageViewController") as? CustomPageViewController else {
            return
        }
        
        // Setting up Delegates
        pageViewController.delegate = self
        pageViewController.dataSource = self
        
        // addiing PageViewCotroller as child to mainViewController
        self.addChild(pageViewController)
        // Change the Parent ViewController of PageViewContrller
        pageViewController.didMove(toParent: self)
        
        pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(pageViewController.view)
        
        let views: [String: Any] = ["pageView": pageViewController.view as Any]
        
       // Adding manual constaints to make pageViewController in View
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[pageView]-0-|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: views))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[pageView]-0-|", options: NSLayoutConstraint.FormatOptions(rawValue: 0), metrics: nil, views: views))
        
        // Get the firstViewController
        guard let startingViewController = feedViewControllerAt(index: currentViewControllerIndex) else {
            return
        }
        
        pageViewController.setViewControllers([startingViewController], direction: .forward, animated: true, completion: nil)
    }
    
    
    // will use this function to determine which controller to display
    func feedViewControllerAt(index: Int) -> FeedViewController? {
        guard let items = self.viewModel?.items else {
            return nil
        }
        
        // if user is at end or at the begining return nil
        if index >= items.count || items.count == 0 {
            return nil
        }
        
        // Create instance of feedViewController
        guard let feedViewController = storyboard?.instantiateViewController(identifier: "FeedViewController") as? FeedViewController else {
            return nil
        }
        
        // give necessary data to FeedViewController
        feedViewController.currentIndex = index
        feedViewController.authorName = items[index].author
        feedViewController.feedTitle = items[index].title
        feedViewController.feedImageName = items[index].media.imgaeLink
        
        return feedViewController
    }

}


extension ViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return currentViewControllerIndex
    }
    
    // determine how many pages will be there
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return viewModel?.items?.count ?? 0
    }
    
    // get pervious VC
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let feedViewController = viewController as? FeedViewController
        
        guard var currentIndex = feedViewController?.currentIndex else {
            return nil
        }
        
        currentViewControllerIndex = currentIndex
        
        if currentIndex == 0 {
            return nil
        }
        currentIndex -= 1
        
        return feedViewControllerAt(index: currentIndex)
    }
    
    // Get next VC
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let feedViewController = viewController as? FeedViewController
        
        guard var currentIndex = feedViewController?.currentIndex, let items = viewModel?.items else {
            return nil
        }
        
        currentViewControllerIndex = currentIndex
        
        if currentIndex == items.count {
            return nil
        }
        currentIndex += 1
        
        return feedViewControllerAt(index: currentIndex)
    }
    
    
}

