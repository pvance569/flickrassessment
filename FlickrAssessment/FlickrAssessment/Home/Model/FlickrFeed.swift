//
//  FlickrFeed.swift
//  FlickrAssessment
//
//  Created by Patel, Virenkumar on 11/17/19.
//  Copyright © 2019 Patel, Virenkumar. All rights reserved.
//

import Foundation

struct FlickrFeed: Codable {
    let title: String
    let items: [Item]?
}

struct Item: Codable {
    let title: String?
    let media: Media
    let author: String?
}

struct Media: Codable {
    let imgaeLink: String?
    
    enum CodingKeys: String, CodingKey {
         case imgaeLink = "m"
     }
}
