//
//  FeedsDAO.swift
//  FlickrAssessment
//
//  Created by Patel, Virenkumar on 11/17/19.
//  Copyright © 2019 Patel, Virenkumar. All rights reserved.
//

import Foundation

typealias FlickrFeedResponse = (FlickrFeed?) -> Void

protocol FlickrFeedResultsProtocol {
    func flickrFeedResults(completion: @escaping FlickrFeedResponse)
}

struct FlickrFeedDAO: FlickrFeedResultsProtocol {
    func flickrFeedResults(completion: @escaping FlickrFeedResponse) {
        
        let url = CredentialsHelper.sharedInstance().publicFeedsURL()
        let params = ["format": "json", "nojsoncallback": "1"] as [String: AnyObject]
        
        let request = RequestConfig(type: .Data, method: .Get, apiEndpoint: url, params: params, httpBody: nil, headers: [:], background: true)
        
        FKRequest(request) { (data, response, error) in
            if let error = error {
                print("FlickrFeedDAO:: ERROR  Unable to complete request on endpoint: \(url) \nError: \(error)")
                completion(nil)
            }
            
            guard let flickrData = data else {
                completion(nil)
                return
            }
            do {
                 let flickrFeed = try JSONDecoder().decode(FlickrFeed.self, from: flickrData)
                completion(flickrFeed)
            } catch {
                print("FlickrFeedDAO:: Unable to decode FlickrFeed Response. Error is \(error)")
                completion(nil)
            }
        }
    }
}
