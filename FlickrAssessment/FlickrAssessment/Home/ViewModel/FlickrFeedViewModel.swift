//
//  FlickrFeedViewModel.swift
//  FlickrAssessment
//
//  Created by Patel, Virenkumar on 11/17/19.
//  Copyright © 2019 Patel, Virenkumar. All rights reserved.
//

import Foundation


struct FlickrFeedViewModel {
    
    var feed: FlickrFeed?
    
    var items: [Item]? {
        return self.feed?.items
    }
    
    // More Coomplex Business logic related to Model comes over here..
    // Ex. Determining what to dispay for label based on three-four defferent variables.
    
}
