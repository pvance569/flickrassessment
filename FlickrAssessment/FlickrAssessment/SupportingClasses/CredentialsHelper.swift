//
//  CredentialsHelper.swift
//  FlickrAssessment
//
//  Created by Patel, Virenkumar on 11/17/19.
//  Copyright © 2019 Patel, Virenkumar. All rights reserved.
//

import Foundation


class CredentialsHelper: NSObject {
    class func sharedInstance() -> CredentialsHelper {
        struct Instance {
            static let credentialsHelper = CredentialsHelper()
        }
        return Instance.credentialsHelper
    }
    
    func baseUrl() -> String {
        return "https://api.flickr.com/services"
    }
    
    func publicFeedsURL() -> String {
        return baseUrl() + "/feeds/photos_public.gne"
    }
    
    // For Future Use to pass default Parameters to request
    func fkrDefaultParams() -> [String:String] {
        
//        var params : [String:String] = [:]
//        return params
        return [:]
    }
}
