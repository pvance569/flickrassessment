//
//  FKRRequest.swift
//  FlickrAssessment
//
//  Created by Patel, Virenkumar on 11/17/19.
//  Copyright © 2019 Patel, Virenkumar. All rights reserved.
//

import Foundation

import Foundation
typealias FKRequestCompletionBlock =  (_ data: Data?, _ response: URLResponse?, _ error: Error?) -> Void

enum HttpMethod: String {
    case Get = "GET", Put = "PUT", Post = "POST" , Delete = "DELETE"
}
enum RequestType {
    case Data, Download, Upload
}

class CFSession {
    
    var session: URLSession?
    
    static let shared = CFSession()
    
    private init() {
        if session == nil {
            session = CFSession.defaultSession()
        }
    }
    
    func invalidateAll() {
        session?.finishTasksAndInvalidate()
        session = nil
    }
    
    static func defaultSession() -> URLSession {
        let configuration = URLSessionConfiguration.default
        configuration.httpCookieAcceptPolicy = .never
        configuration.httpShouldSetCookies = false
        let session = URLSession(configuration: configuration)
        return session
    }
}


// User RequestConfig to pass necessary components for creating URLRequest
struct RequestConfig {
    
    let type: RequestType
    let method: HttpMethod
    let apiEndpoint: String
    let headers: [String:String]
    let params: [String:AnyObject?]?
    let httpBody: Data?
    let background: Bool
    
    // For GET pass params
    // For POST or PUT pass httpBody
    init(type: RequestType = .Data,
         method:HttpMethod = .Get,
         apiEndpoint:String,
         params: [String:AnyObject?]? = nil,
         httpBody: Data? = nil,
         headers: [String:String] = [:],
         background: Bool = false) {
        
        self.type = type
        self.method = method
        self.apiEndpoint = apiEndpoint
        self.httpBody = httpBody
        self.params = params
        self.headers = headers
        self.background = background
    }
}

class FKRequest {
    
    let config : RequestConfig
    
    @discardableResult init(_ config: RequestConfig,
                            completion: @escaping FKRequestCompletionBlock) {
        self.config = config
        fkrequest(completion)
    }
    
    private func fkrequest(_ completion: @escaping FKRequestCompletionBlock) {
        
        switch config.type {
        case .Data:
            
            if let request = createRequest(self.config) {
                
                data(request, config.background, completion: {
                    (data, response, error) in
                    DispatchQueue.main.async {
                        completion(data, response, error)
                    }
                })
                
            } else {
                print("ERROR - Please check for valid parameters in FKRequest")
            }
            
        case .Download:
            // Create request for heavy downloading
            break
        case .Upload:
            // Create Request for uploading and handle up here.
            break
        }
        
    }
    
    private func createRequest(_ config: RequestConfig) -> URLRequest? {
        
        guard let url = URL(string: config.apiEndpoint) else {
            return nil
        }
        
        if config.method == .Get {
            
            if let body = self.config.params{
                var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)
                
                var targetDict = [String: String]()
                for (key, value) in body {
                    if let value = value as? String { targetDict[key] = value }
                }
                
                let queryItems = targetDict.map {
                    return URLQueryItem(name: "\($0)", value: "\($1)")
                }
                
                urlComponents?.queryItems = queryItems
                var localVariable = urlComponents
                urlComponents?.percentEncodedQuery = localVariable?.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
                
                var request = URLRequest(url: (urlComponents?.url)!)
                
                
                request.httpMethod = config.method.rawValue
                self.config.headers.forEach({
                    request.setValue($0.value, forHTTPHeaderField: $0.key)
                })
                
                
                
                print("REQUEST: \(String(describing: request))")
                return request
                
            }
            //No params present
            var request = URLRequest(url: url)
            
            request.httpMethod = config.method.rawValue
            self.config.headers.forEach({
                request.setValue($0.value, forHTTPHeaderField: $0.key)
            })
            return request
            
        } else {
            // Other request Type..
        }
        
        return nil
    }
    
    private func data(_ request: URLRequest, _ background: Bool = false, completion: @escaping FKRequestCompletionBlock) {
        
        if CFSession.shared.session == nil {
            CFSession.shared.session = CFSession.defaultSession()
        }
        
        if !background {
            // TODO:: add loading indicator.
            // use this to track global CarFax app service activity
        }
        
        let task = CFSession.shared.session?.dataTask(with: request, completionHandler: { (data, response, error) in
            
            if error != nil {
                
                print("\nError: Unable to complete request on: \(self.config.apiEndpoint)")
                print(error.debugDescription)
                
                completion(nil, response, error)
                
            } else {
                
                completion(data, response, error)
                
            }
            if !background {
                // TODO:: Disable loading indicator.
                // use this to track global CarFax app service activity
            }
            
        })
        task?.resume()
    }
}
