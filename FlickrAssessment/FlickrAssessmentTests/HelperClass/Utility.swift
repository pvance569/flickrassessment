//
//  Utility.swift
//  FlickrAssessmentTests
//
//  Created by Patel, Virenkumar on 11/17/19.
//  Copyright © 2019 Patel, Virenkumar. All rights reserved.
//

import Foundation


class Utility {
    func getMockData(forResource: String) -> Data? {
        let currentBundle = Bundle(for: type(of: self))
        if let pathForMock = currentBundle.url(forResource: forResource, withExtension: "json") {
            do {
                return try Data(contentsOf: pathForMock)
            } catch {
                print("Unable to convert \(pathForMock) to Data.")
                return nil
            }
        } else {
            print("Unable to load \(forResource).json")
            return nil
        }
    }
}

