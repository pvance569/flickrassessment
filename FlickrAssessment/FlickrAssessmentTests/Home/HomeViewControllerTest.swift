//
//  HomeViewControllerTest.swift
//  FlickrAssessmentTests
//
//  Created by Patel, Virenkumar on 11/17/19.
//  Copyright © 2019 Patel, Virenkumar. All rights reserved.
//

import XCTest
@testable import FlickrAssessment

class HomeViewControllerTest: XCTestCase {
    
    var viewController: ViewController?

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        viewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as? ViewController
        
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testHomeViewController() {
        
        if let vc = self.viewController {
            let _ = vc.view
            vc.fetchFeeds(dataAccessObjet: FlickrFeedMockDAO())
            XCTAssertNotNil(self.viewController?.viewModel?.items?.count, "Items in ViewModel is. Please investigate - Vance")
            // Fertching VC at index 3
            let thirdVC = vc.feedViewControllerAt(index: 3)
            XCTAssertNotNil(thirdVC)
            XCTAssertEqual(thirdVC?.authorName, "nobody@flickr.com (\"Angie Ward Online\")")
            XCTAssertNil(vc.feedViewControllerAt(index: 21), "FeedViewController should be nil at 21. ")
            
            // Creating instance of PageViewController
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            guard let pageViewController = storyboard.instantiateViewController(identifier: "CustomPageViewController") as? CustomPageViewController else {
                return
            }
            
            if let thirdVC = thirdVC {
                // Getting FourthVC using delegate
                let fourthVC = vc.pageViewController(pageViewController, viewControllerAfter: thirdVC)
                XCTAssertNotNil(fourthVC)
                if let fourthVC = fourthVC as? FeedViewController {
                    XCTAssertEqual(fourthVC.authorName, "nobody@flickr.com (\"donsathletics\")")
                } else {
                    XCTFail("Couldn't get Fourth VC")
                }
                
                // Getting SecondVC using Delegate
                let secondVC = vc.pageViewController(pageViewController, viewControllerBefore: thirdVC)
                XCTAssertNotNil(secondVC)
                if let secondVC = secondVC as? FeedViewController {
                    XCTAssertEqual(secondVC.feedImageName, "https://live.staticflickr.com/65535/49080165928_f5ddf5885e_m.jpg")
                } else {
                    XCTFail("Couldn't Get Second VC")
                }
            }
        } else {
            XCTFail("ViewController is nil.")
        }
    }

}
