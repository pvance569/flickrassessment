//
//  FlickrFeedMockDAO.swift
//  FlickrAssessmentTests
//
//  Created by Patel, Virenkumar on 11/17/19.
//  Copyright © 2019 Patel, Virenkumar. All rights reserved.
//

import Foundation
@testable import FlickrAssessment

struct FlickrFeedMockDAO: FlickrFeedResultsProtocol {
    func flickrFeedResults(completion: @escaping FlickrFeedResponse) {
        guard let flickrFeedData = Utility().getMockData(forResource: "FlickrFeed") else {
            completion(nil)
            return
        }
        
        do {
            let jsonDecoder = JSONDecoder()
            
            //decode mock into data structure
            let flickrFeed = try jsonDecoder.decode(FlickrFeed.self, from: flickrFeedData)
            completion(flickrFeed)
            
        } catch  {
            completion(nil)
        }
    }
    
}
